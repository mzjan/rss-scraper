#!/bin/bash
python3 /var/www/rss_scraper/manage.py makemigrations;
python3 /var/www/rss_scraper/manage.py migrate;
python3 /var/www/rss_scraper/manage.py collectstatic;
python3 /var/www/rss_scraper/manage.py fill_currency_dict;
python3 /var/www/rss_scraper/manage.py create_test_user;
service redis-server start;
a2dissite 000-default.conf
a2ensite rss_apache2.conf
service apache2 start;
chown -R www-data:www-data /var/www/rss_scraper
cd /var/www/rss_scraper;
celery -A rss_scraper beat -l info --detach
celery -A rss_scraper worker -l info --detach
tail -f /var/log/apache2/error.log
