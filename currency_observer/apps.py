from django.apps import AppConfig


class CurrencyObserverConfig(AppConfig):
    name = 'currency_observer'
