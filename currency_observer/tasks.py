from __future__ import absolute_import, unicode_literals
from celery import task
from django.db import IntegrityError

from django.utils.dateparse import parse_datetime
import feedparser

from currency_observer import models
from django.conf import settings


def get_parsed_ecb_reference_rate_by_currency(curr):
    rss_url = settings.ECB_RSS_URL.format(curr.lower())
    rss_feed = feedparser.parse(rss_url)
    parsed_rss_feed = []
    for e in rss_feed['entries']:
        parsed_entry = {}
        parsed_entry['id'] = e['id']
        parsed_entry['title'] = e['title']
        parsed_entry['exchange_rate'] = e['cb_exchangerate'].replace('\nEUR', '')
        parsed_entry['updated'] = e['updated']
        parsed_rss_feed.append(parsed_entry)
    return parsed_rss_feed


@task()
def update_euro_reference_rates():

    for curr in settings.ECB_CURRENCIES.keys():
        curr_rss_feed = get_parsed_ecb_reference_rate_by_currency(curr)
        for e in curr_rss_feed:
            try:
                euro_ref_rate = models.EuroReferenceRate()
                euro_ref_rate.guid = e['id']
                euro_ref_rate.title = e['title']
                euro_ref_rate.exchange_rate = e['exchange_rate']
                euro_ref_rate.updated_timestamp = parse_datetime(e['updated'])
                target_curr = models.CurrencyDictionary.objects.get(currency=curr.upper())
                euro_ref_rate.target_currency = target_curr
                euro_ref_rate.save()

            except IntegrityError:
                # this error occurs when guid unique constraint failed
                continue
