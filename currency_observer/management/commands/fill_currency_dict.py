from django.core.management.base import BaseCommand
from django.db import IntegrityError

from rss_scraper import settings
from currency_observer.models import CurrencyDictionary

class Command(BaseCommand):
    help = 'Fill CurrencyDictionary model with default currency list.'

    def handle(self, *args, **kwargs):

        for curr_symbol, curr_name in settings.ECB_CURRENCIES.items():
            try:
                c = CurrencyDictionary()
                c.name = curr_name
                c.currency = curr_symbol
                c.save()
            except IntegrityError:
                continue

        self.stdout.write("CurrencyDictionary is filled with values!")
