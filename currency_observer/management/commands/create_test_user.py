from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.db import IntegrityError


class Command(BaseCommand):
    help = 'Creates test superuser.'

    def handle(self, *args, **kwargs):

        try:
            user = User.objects.create_user('test', password='test')
            user.is_superuser = True
            user.is_staff = True
            user.save()
            self.stdout.write("User added!")

        except IntegrityError:
            self.stdout.write("Username test is already taken!")
