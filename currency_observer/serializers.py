from rest_framework import serializers

from currency_observer.models import EuroReferenceRate, CurrencyDictionary


class CurrencyDictionarySerializer(serializers.ModelSerializer):
    class Meta:
        model = CurrencyDictionary
        fields = ['currency', 'name']


class EuroReferenceRateSerializer(serializers.ModelSerializer):
    class Meta:
        model = EuroReferenceRate
        target_currency = CurrencyDictionarySerializer()
        fields = ['guid', 'title', 'target_currency', 'exchange_rate',
                  'updated_timestamp']
