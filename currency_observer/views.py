from rest_framework import generics, viewsets

from currency_observer.models import EuroReferenceRate, CurrencyDictionary
from currency_observer import serializers


class EuroReferenceRateViewSet(viewsets.ModelViewSet):
    queryset = EuroReferenceRate.objects.all()
    serializer_class = serializers.EuroReferenceRateSerializer


class CurrencyDictionaryViewSet(viewsets.ModelViewSet):
    queryset = CurrencyDictionary.objects.all()
    serializer_class = serializers.CurrencyDictionarySerializer


class EuroReferenceRateList(generics.ListAPIView):
    serializer_class = serializers.EuroReferenceRateSerializer

    def get_queryset(self):
        curr = self.kwargs['currency']
        return EuroReferenceRate.objects.filter(target_currency__currency=curr)
