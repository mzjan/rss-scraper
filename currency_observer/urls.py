from django.conf.urls import url

from currency_observer import views

app_name = 'curr_observer'

urlpatterns = [
    url('^filter/(?P<currency>.+)/$', views.EuroReferenceRateList.as_view(), name='curr_rate'),
]
