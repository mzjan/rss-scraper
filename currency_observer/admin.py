from django.contrib import admin

from currency_observer import models


@admin.register(models.EuroReferenceRate)
class EuroReferenceRateAdmin(admin.ModelAdmin):
    list_display = ('title', 'target_currency', 'exchange_rate',
                    'updated_timestamp')
    ordering = ('updated_timestamp', 'title')


@admin.register(models.CurrencyDictionary)
class CurrencyDictionaryAdmin(admin.ModelAdmin):
    list_display = ('currency', 'name')
    ordering = ('currency',)
