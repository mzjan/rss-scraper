from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status

from currency_observer import models

# initialize the APIClient app
client = Client()


class CurrencyDictionary(TestCase):
    """ Test module for CurrencyDictionary model """

    def setUp(self):
        usd = models.CurrencyDictionary.objects.create(currency='USD',
                                                       name='US Dollar')
        models.EuroReferenceRate.objects.create(
            guid='http://www.ecb.europa.eu/stats/exchange/eurofxref/html'
                 '/eurofxref-graph-usd.en.html?date=2020-01-31&rate=1.1052',
            title='1.1052 USD = 1 EUR 2020-01-31 ECB Reference rate',
            target_currency=usd,
            exchange_rate='1.1052',
            updated_timestamp='2020-01-31T13:15:00Z',
        )

    def test_api_get_currency_rate(self):
        target_url = reverse('curr_observer:curr_rate',
                             kwargs={'currency': 'USD'})
        response = client.get(target_url)
        results = [x for x in response.json()['results']
                   if x['guid'] == 'http://www.ecb.europa.eu/stats/exchange' \
                                   '/eurofxref/html/eurofxref-graph-usd.en.html'\
                                   '?date=2020-01-31&rate=1.1052'][0]

        self.assertEqual(results['title'], '1.1052 USD = 1 EUR 2020-01-31 ECB '
                                           'Reference rate')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
