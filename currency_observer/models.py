from django.db import models


class CurrencyDictionary(models.Model):
    currency = models.CharField(max_length=3, unique=True)  # ISO 4217
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.currency

    class Meta:
        ordering = ['name']


class EuroReferenceRate(models.Model):
    """The reference rates are based on the regular daily concertation procedure
     between central banks within and outside the European System of Central
     Banks, which normally takes place at 2.15 p.m. (14:15) ECB time."""
    guid = models.CharField(max_length=256, unique=True)
    title = models.CharField(max_length=250)
    target_currency = models.ForeignKey(CurrencyDictionary,
                                        on_delete=models.CASCADE)
    exchange_rate = models.DecimalField(max_digits=24, decimal_places=4)
    updated_timestamp = models.DateTimeField()

    class Meta:
        ordering = ['id']
