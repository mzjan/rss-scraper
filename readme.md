# RSS scraper
This project contains scraper RSS of ECB currencies rates. It continuously update database
and put records into database. It also provides API endpoint returning history of all
reference rates records for selected currency.

Architecture: \
As scheduler and task runner for updating records from ECB Celery+Redis were used. It downloads date each ones
each minute, it is obviously to often, but it was set for testing. \
For parsing RSS feedparser was used. \
For providing API django and django rest framework was used. \
To make this app easier on deploy Docker was used. 

**How to run this?**\
To run this project just build and run Dockerfile: copy whole project to some directory, go to this directory and run: 

`docker build -t debian;`\
`docker run -d -p 80:80 debian;`

The demo app will be available on localhost:80. The demo app has one API url
 which allows to get all history of reference rates of selected currency.
 
 localhost/api/filter/USD -- example url for Dollar \
 localhost/api/filter/PLN -- example url for Polish zloty
 
 After running container there is also access to django admin site,
 the test user was set, and you can log in to admin panel. \
 user: test \
 pass: test
 