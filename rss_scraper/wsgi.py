"""
WSGI config for rss_scraper project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os
from sys import path

from django.core.wsgi import get_wsgi_application
path.append("/var/www/rss_scraper") # this is tweak needed to run properly import
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'rss_scraper.settings')

application = get_wsgi_application()
