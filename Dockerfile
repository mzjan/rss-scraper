FROM debian:latest

RUN apt-get update

#ENV LC_ALL en_US.UTF-8
#ENV LANG en_US.UTF-8
#ENV LANGUAGE en_US.UTF-8
ENV PYTHONIOENCODING=utf-8

RUN apt-get install apache2 -y
RUN apt-get install libapache2-mod-wsgi-py3 -y
RUN apt-get install python3-pip -y
RUN apt-get install redis-server -y

RUN pip3 install Django==2.2
RUN pip3 install djangorestframework==3.11.0
RUN pip3 install feedparser==5.2.1
RUN pip3 install Celery==4.4.0
RUN pip3 install redis==3.3.11

# apache2 sys envs
ENV APACHE_CONFDIR /etc/apache2
ENV APACHE_ENVVARS $APACHE_CONFDIR/envvars
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_RUN_DIR /var/run/apache2
ENV APACHE_PID_FILE $APACHE_RUN_DIR/apache2.pid
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_LOG_DIR /var/log/apache2
ENV LANG C

RUN a2enmod wsgi

COPY currency_observer /var/www/rss_scraper/currency_observer/.
COPY rss_scraper /var/www/rss_scraper/rss_scraper/.
COPY manage.py /var/www/rss_scraper/.
COPY scripts/rss_apache2.conf /etc/apache2/sites-available/.
COPY scripts/start.sh /.
RUN chmod +x /start.sh

EXPOSE 80

ENTRYPOINT ["/start.sh"]